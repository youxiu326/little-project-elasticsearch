package com.ruyuan.little.project.elasticsearch.biz.admin.dto;

import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsSpu;
import lombok.Data;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:elasticsearch实战
 **/
@Data
public class AdminGoodsSpuAddDTO {
    /**
     * 店铺id
     */
    private String storeId;

    /**
     * 商品spu信息
     */
    private AdminGoodsSpu adminGoodsSpu;
}
